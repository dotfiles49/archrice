#!/bin/bash
while :; do
	dwm -s "$(sensors | awk '/^Tctl/ {print $2}' | sed 's/+//') | $(free -h | awk '/^Mem:/ {print $3 "/" $2}') | $(df -h | awk '/^\/dev\/sda/ {print $3 "/" $2}') | $(date "+%a %d/%m/%Y %H:%M")"
	sleep 1m
done
