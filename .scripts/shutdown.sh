#!/bin/bash

chosen=$(echo -e "Yes\nNo" | dmenu -i -p "Are you sure you want to shut down?")
[[ "$chosen" = "" ]] && exit 1
if [[ $chosen == "Yes" ]]; then
        sudo poweroff
fi
if [[ $chosen == "No" ]]; then
	exit 0
fi
